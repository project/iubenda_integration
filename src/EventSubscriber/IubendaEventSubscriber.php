<?php

namespace Drupal\iubenda_integration\EventSubscriber;

use Drupal\Core\Routing\AdminContext;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event Subscriber IubendaEventSubscriber.
 */
class IubendaEventSubscriber implements EventSubscriberInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Routing\AdminContext $adminContext
   *   The admin context.
   */
  public function __construct(
    protected readonly AdminContext $adminContext,
    ) {
  }

  /**
   * Parse the rendered HTML.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The subscribed event.
   */
  public function onKernelResponse(ResponseEvent $event) {
    $response = $event->getResponse();
    $request = $event->getRequest();

    // Do not capture redirects or modify XML HTTP Requests.
    if ($request->isXmlHttpRequest()) {
      return;
    }

    // Exclude files.
    if ($response instanceof BinaryFileResponse) {
      return;
    }

    // Parse non administrative pages only.
    if (!$response->isEmpty() && !$this->adminContext->isAdminRoute()) {

      // Parse all page's HTML and check for cookies intent lock.
      // For more informations go to https://www.iubenda.com/en/help/posts/1976.
      if (!\iubendaParser::consent_given() && !\iubendaParser::bot_detected()) {
        $iubenda = new \iubendaParser($response->getContent(), [
          'type' => 'page',
        ]);
        $response->setContent($iubenda->parse());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::RESPONSE => ['onKernelResponse', -128],
    ];
  }

}
