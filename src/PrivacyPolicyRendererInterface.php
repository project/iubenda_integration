<?php

declare(strict_types=1);

namespace Drupal\iubenda_integration;

use Drupal\Core\GeneratedLink;

/**
 * This class contains methods to manage the privacy policy rendering.
 *
 * @group iubenda_integration
 */
interface PrivacyPolicyRendererInterface {

  /**
   * Builds the Iubenda privacy policy link.
   *
   * @param string
   *   The link text.
   *
   * @return \Drupal\Core\GeneratedLink
   *   Iubenda privacy policy link as HTML markup.
   */
  public function buildLink(string $link_text = ''): GeneratedLink;

  /**
   * Build the Iubenda privacy policy text.
   *
   * @return string
   *   A string containing the configured Iubenda pre-text, link and post-text.
   */
  public function buildString(): string;

}
