<?php

declare(strict_types=1);

namespace Drupal\iubenda_integration;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\GeneratedLink;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * This class contains methods to manage the privacy policy rendering.
 *
 * @group iubenda_integration
 */
class PrivacyPolicyRenderer implements PrivacyPolicyRendererInterface {

  /**
   * The class constructor.
   *
   * @param ImmutableConfig $config
   *   The configuration.
   */
  public function __construct(
    private readonly ConfigFactory $config,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function buildLink(string $link_text = ''): GeneratedLink {
    $settings = $this->config->get('iubenda_integration.settings');
    $link_text = ($link_text === '') ? $settings->get('iubenda_integration_text') : $link_text;
    $code = $settings->get('iubenda_integration_policy_code');
    $link_classes = [
      $settings->get('iubenda_integration_style'),
      'iubenda-embed',
      'iubenda-noiframe',
    ];

    if ($settings->get('iubenda_integration_legal_only')) {
      $link_classes[] = 'iub-legal-only';
    }

    if (!$settings->get('iubenda_integration_show_brand')) {
      $link_classes[] = 'no-brand';
    }

    $link_options = [
      'external' => TRUE,
      'attributes' => [
        'class' => $link_classes,
        'title' => trim($link_text),
      ],
    ];

    $iubenda_url = Url::fromUri(IUBENDA_INTEGRATION_PRIVACY_POLICY_URL . $code);
    $iubenda_url->setOptions($link_options);

    return Link::fromTextAndUrl($link_text, $iubenda_url)->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function buildString(): string {
    $settings = $this->config->get('iubenda_integration.settings');
    $pre_text = $settings->get('iubenda_integration_pretext');
    $post_text = $settings->get('iubenda_integration_posttext');
    $middle_text = $this->buildLink();

    return trim($pre_text) . ' ' . $middle_text . ' ' . trim($post_text);
  }

}
