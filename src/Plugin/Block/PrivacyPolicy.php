<?php

namespace Drupal\iubenda_integration\Plugin\Block;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\iubenda_integration\PrivacyPolicyRendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the PrivacyPolicy block.
 *
 * @Block(
 *   id = "iubenda_integration_privacy_policy",
 *   admin_label = @Translation("Iubenda Integration: Privacy policy")
 * )
 */
class PrivacyPolicy extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  final public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly PrivacyPolicyRendererInterface $privacyPolicyRenderer,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $privacy_policy_renderer = $container->get('iubenda_integration.privacy_policy.renderer');
    assert($privacy_policy_renderer instanceof PrivacyPolicyRendererInterface);

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $privacy_policy_renderer,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'iubenda_integration_block' =>
        [
          'text_prefix' => '',
          'text' => $this->t('Privacy policy'),
          'text_suffix' => '',
        ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $configurations_iubenda = $this->configuration['iubenda_integration_block'];

    $form['iubenda_integration_block'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Iubenda privacy policy settings'),
    ];
    $form['iubenda_integration_block']['text_prefix'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Link prefix'),
      '#description' => $this->t('Insert the text before the Privacy Policy link.'),
      '#default_value' => $configurations_iubenda['text_prefix'],
    ];
    $form['iubenda_integration_block']['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link text'),
      '#description' => $this->t('Insert text that will be displayed as the link.'),
      '#default_value' => $configurations_iubenda['text'],
      '#required' => TRUE,
    ];
    $form['iubenda_integration_block']['text_suffix'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Link suffix'),
      '#description' => $this->t('Insert the text after the Privacy Policy link.'),
      '#default_value' => $configurations_iubenda['text_suffix'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $configurations = $form_state->getValue('iubenda_integration_block');
    $this->configuration['iubenda_integration_block'] = $configurations;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'block__iubenda_privacy_policy',
      '#pre_text' => Xss::filterAdmin($this->configuration['iubenda_integration_block']['text_prefix']),
      '#link' => $this->privacyPolicyRenderer->buildLink($this->configuration['iubenda_integration_block']['text']),
      '#post_text' => Xss::filterAdmin($this->configuration['iubenda_integration_block']['text_suffix']),
    ];
  }

}
