<?php

namespace Drupal\iubenda_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures iubenda_integration settings.
 */
class IubendaSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'iubenda_integration_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'iubenda_integration.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $iubenda_config = $this->config('iubenda_integration.settings');

    $form['settings'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Iubenda privacy policy'),
    ];

    // Iubenda tab.
    $form['iubenda'] = [
      '#type' => 'details',
      '#title' => $this->t('General'),
      '#group' => 'settings',
    ];
    $form['iubenda']['iubenda_integration_policy_code'] = [
      '#title' => $this->t('Iubenda Privacy Policy code'),
      '#type' => 'textfield',
      '#description' => $this->t('Insert the privacy policy code generated
        by Iubenda for your website. You can find it as latest element in your
        privacy URL (ex. https://www.iubenda.com/privacy-policy/XXXXXXX).'),
      '#default_value' => $iubenda_config->get('iubenda_integration_policy_code'),
      '#required' => TRUE,
      '#size' => 10,
    ];
    $form['iubenda']['iubenda_integration_style'] = [
      '#title' => $this->t('Style'),
      '#type' => 'radios',
      '#options' => [
        'iubenda-nostyle' => $this->t('No style'),
        'iubenda-white' => $this->t('White'),
        'iubenda-black' => $this->t('Black'),
      ],
      '#default_value' => $iubenda_config->get('iubenda_integration_style'),
      '#required' => TRUE,
    ];
    $form['iubenda']['iubenda_integration_legal_only'] = [
      '#title' => $this->t('Show legal only'),
      '#type' => 'checkbox',
      '#default_value' => $iubenda_config
        ->get('iubenda_integration_legal_only'),
    ];
    $form['iubenda']['iubenda_integration_show_brand'] = [
      '#title' => $this->t('Show brand'),
      '#type' => 'checkbox',
      '#default_value' => $iubenda_config
        ->get('iubenda_integration_show_brand'),
    ];

    // Form tab.
    $form['form'] = [
      '#type' => 'details',
      '#title' => $this->t('Form'),
      '#group' => 'settings',
    ];
    $form['form']['iubenda_integration_form_element_type'] = [
      '#title' => $this->t('Form element type'),
      '#type' => 'select',
      '#options' => [
        'checkbox' => $this->t('Checkbox'),
        'radio' => $this->t('Radio'),
      ],
      '#default_value' => $iubenda_config
        ->get('iubenda_integration_form_element_type'),
    ];
    $form['form']['iubenda_integration_form_element_label'] = [
      '#title' => $this->t('Form element label'),
      '#description' => $this->t('This text will be displayed as label of the input form element to accept the privacy policy. "Yes" is the default label.'),
      '#type' => 'textfield',
      '#default_value' => $iubenda_config->get('iubenda_integration_form_element_label') ?? 'Yes',
      '#required' => TRUE,
    ];
    $form['form']['iubenda_integration_forms'] = [
      '#title' => $this->t('Privacy Policy Forms'),
      '#description' => $this->t('Insert here the form ids list (one per line)
        where you need to apply the Iubenda Privacy Policy.'),
      '#type' => 'textarea',
      '#default_value' => $iubenda_config->get('iubenda_integration_forms'),
    ];
    $form['form']['text'] = [
      '#title' => $this->t('Privacy Policy link'),
      '#type' => 'fieldset',
    ];
    $form['form']['text']['iubenda_integration_pretext'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Link prefix'),
      '#description' => $this->t('Insert the text before the Privacy Policy link.'),
      '#default_value' => $iubenda_config->get('iubenda_integration_pretext'),
    ];
    $form['form']['text']['iubenda_integration_text'] = [
      '#title' => $this->t('Link text'),
      '#description' => $this->t('Insert text that will be displayed as the link.'),
      '#type' => 'textfield',
      '#default_value' => $iubenda_config->get('iubenda_integration_text'),
      '#required' => TRUE,
    ];
    $form['form']['text']['iubenda_integration_posttext'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Link suffix'),
      '#description' => $this->t('Insert the text after the Privacy Policy link.'),
      '#default_value' => $iubenda_config->get('iubenda_integration_posttext'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save settings.
    foreach ($form_state->getValues() as $key => $value) {
      $this->config('iubenda_integration.settings')
        ->set($key, $value)
        ->save();
    }

    parent::submitForm($form, $form_state);
  }

}
