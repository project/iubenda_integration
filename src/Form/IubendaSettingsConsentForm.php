<?php

namespace Drupal\iubenda_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures iubenda_integration settings.
 */
class IubendaSettingsConsentForm extends ConfigFormBase {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->configFactory = $container->get('config.factory');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'iubenda_integration_settings_consent';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'iubenda_integration.settings.consent',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('iubenda_integration.settings');

    $form['settings'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['consent_solution_general'] = [
      '#type' => 'details',
      '#title' => 'General',
      '#group' => 'settings',
      '#weight' => 0,
    ];
    $form['consent_solution_general']['api_key'] = [
      '#title' => $this->t('Api key'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 60,
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t('Your API key. You can find it
        in your Iubenda dashboard javascript code (ex. {api_key: "x0XXXXxXXXxXXxXxxXXxx6Xxx0x1XXxx"}).'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('iubenda_integration.settings');

    // Save settings.
    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, $form_state->getValue($key));
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
