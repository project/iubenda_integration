<?php

namespace Drupal\iubenda_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures iubenda_integration settings.
 */
class IubendaSettingsCookieForm extends ConfigFormBase {

  const IUBENDA_COOKIE_SOLUTION = 'iubenda_cookie_solution';

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->languageManager = $container->get('language_manager');
    $instance->configFactory = $container->get('config.factory');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'iubenda_integration_settings_cookie';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'iubenda_integration.settings.cookie',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('iubenda_integration.settings');

    $form['settings'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['cookie_solution_general'] = [
      '#type' => 'details',
      '#title' => 'General',
      '#group' => 'settings',
      '#weight' => 0,
    ];
    $form['cookie_solution_general']['siteId'] = [
      '#title' => $this->t('Site ID'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#size' => 10,
      '#default_value' => $config->get('siteId'),
      '#description' => $this->t('Your site identity code. You can find it
        in your embed banner javascript code (ex. "siteId":XXXXXX).'),
    ];
    $form['cookie_solution_general']['consentOnContinuedBrowsing'] = [
      '#title' => $this->t('Consent on continued browsing'),
      '#description' => $this->t("If you're operating, for instance, in
        the <b><i>UK, Ireland, France, Germany, Denmark, Belgium and Greece</i></b>,
        consider that the respective national data protection authorities do not
        regard consent by continuing navigation as valid."),
      '#type' => 'checkbox',
      '#default_value' => $config->get('consentOnContinuedBrowsing') ?? '1',
    ];
    $form['cookie_solution_general']['position'] = [
      '#title' => $this->t('Banner position'),
      '#type' => 'select',
      '#options' => [
        'full-top' => $this->t('Full width top'),
        'bottom' => $this->t('Full width bottom'),
        'float-center' => $this->t('Floating center'),
        'float-top-left' => $this->t('Floating top left'),
        'float-top-center' => $this->t('Floating top center'),
        'float-top-right' => $this->t('Floating top right'),
        'float-bottom-left' => $this->t('Floating bottom left'),
        'float-bottom-center' => $this->t('Floating bottom center'),
        'float-bottom-right' => $this->t('Floating bottom right'),
      ],
      '#default_value' => $config->get('position'),
    ];
    $form['cookie_solution_general']['backgroundOverlay'] = [
      '#title' => $this->t('Background overlay'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('backgroundOverlay'),
    ];

    $form['cookie_solution_general']['buttons'] = [
      '#type' => 'details',
      '#title' => $this->t('Buttons (accept, customize, reject, close)'),
    ];
    $form['cookie_solution_general']['buttons']['acceptButtonDisplay'] = [
      '#title' => $this->t('Accept button'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('acceptButtonDisplay'),
      '#description' => $this->t('Determines whether or not the “Accept” button is displayed.'),
    ];
    $form['cookie_solution_general']['buttons']['customizeButtonDisplay'] = [
      '#title' => $this->t('Customize button'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('customizeButtonDisplay'),
      '#description' => $this->t('Determines whether or not the “Learn more and customize” button is displayed.'),
    ];
    $form['cookie_solution_general']['buttons']['rejectButtonDisplay'] = [
      '#title' => $this->t('Reject button'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('rejectButtonDisplay'),
      '#description' => $this->t('Determines whether or not the “Reject” button is displayed. When <i>true</i>, “Close” button is forced to <i>false</i>.'),
    ];
    $form['cookie_solution_general']['buttons']['closeButtonDisplay'] = [
      '#title' => $this->t('Close button'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('closeButtonDisplay') ?? '1',
      '#description' => $this->t("If set to <i>false</i>, the banner's “Close” button won't be displayed."),
      '#states' => [
        'unchecked' => [
          ':input[name="rejectButtonDisplay"]' => ['checked' => TRUE],
        ],
        'disabled' => [
          ':input[name="rejectButtonDisplay"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['cookie_solution_general']['buttons']['closeButtonRejects'] = [
      '#title' => $this->t('Close button rejects'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('closeButtonRejects'),
      '#description' => $this->t("If set to <i>true</i>, when the banner's “Close” button is clicked, user's consent is considered as denied."),
    ];

    $form['cookie_solution_gstyle'] = [
      '#type' => 'details',
      '#title' => $this->t('Style'),
      '#group' => 'settings',
      '#weight' => 0,
    ];
    $form['cookie_solution_gstyle']['applyStyles'] = [
      '#title' => $this->t('Apply styles'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('applyStyles'),
      '#description' => $this->t("(default true) if set to false, then banner doesn't have any CSS style applied."),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('iubenda_integration.settings');

    // Save settings.
    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, $form_state->getValue($key));
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
