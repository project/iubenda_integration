# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION
This module provides integration between Drupal and Iubenda
https://www.iubenda.com. If you need to add the Iubenda privacy policy to your
forms, or display Iubenda EU Cookie Policy banner, this is the module you are
searching for.
This module already supports usage of Variable and Internationalization modules,
allowing you to translate you Iubenda privacy policy texts.
Iubenda Integration also provides a new block "Iubenda Integration:
Privacy policy", allowing you to say "Why another block?!"... eheh bad stories,
but sometimes could be useful :)

## Features:
 * The module automatically inserts the Iubenda code in the head of every website pages.
 * It handles the display of privacy policy.
 * It handles the display of cookie banners and cookie policy, saving user preferences about the use of cookies.
 * It displays a clean page (without banner) to users who have already provided their consent.
 * It detects bots/spiders and serves them a clean page.

## Status:
 * 4.x - Actively supported.
 * 3.x - Actively supported.
 * 8.x-2.x - No more supported!
 * 7.x-2.x - Bug fixing only.
 * 7.x-1.x - No more supported!

## REQUIREMENTS
Iubenda Integration 8.x-2.x work with:
 * Libraries API >= 2 (https://www.drupal.org/project/libraries)
 * Iubenda PHP Class (http://simon.s3.iubenda.com/iubenda-cookie-class)


## INSTALLATION

### Drupal 9/10
Install using composer. Run this command in your project root folder:
composer require drupal/iubenda_integration
Iubenda PHP class iubenda/iubenda-cookie-class will be automatically included.

### Drupal 7
Install as you would normally install a contributed Drupal module. See: https://drupal.org/documentation/install/modules-themes/modules-7 for further information.
Download and install the Libraries API 2 module and the Iubenda Integration module as normal. Then download the Iubenda PHP class for cookies which allows blocking prior to consent.
Download Iubenda PHP class: https://github.com/iubenda/iubenda-cookie-class/releases
Unpack and rename the class directory to "iubenda-cookie-class", afterward place it inside the "sites/all/libraries" directory on your server.
Make sure the path to the class file becomes: "sites/all/libraries/iubenda-cookie-class-4.1.8/iubenda.class.php"
Install with Drush
The easiest way to download and install the module and the PHP class is via the built in Drush commands
drush dl iubenda_integration (download the module)
drush en iubenda_integration (enable the module, download and install the
Iubenda PHP class)
drush iubenda-php-class (download and install the Iubenda PHP class)

If you use a drush make workflow, Drush automatically download the library using the iubenda_integration.make file.

## CONFIGURATION
 * Enable Iubenda Integration module and configure it from "admin/config/system/iubenda-integration"
 * Do you really need a privacy policy block?! Wow, you can configure it tough ;)
 * Enjoy it!

## MAINTAINERS
Current maintainers:
 * Daniele Piaggesi (g0blin79) - https://www.drupal.org/u/g0blin79
 * Roberto Peruzzo (robertoperuzzo) - https://www.drupal.org/u/robertoperuzzo
 * Nikolas Costa (nikolas.costa) - https://www.drupal.org/u/nikolascosta

Module development and maintenance by:
 * BMEME
   The Drupal Factory.
   Visit http://www.bmeme.com for more information.

 * STUDIO AQUA
   Drupal with web marketing around.
   Visit http://www.studioaqua.it for more information.
